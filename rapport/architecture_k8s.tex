\chapter{Présentation de l'architecture Kubernetes}
Si vous connaissez les bases de Kubernetes, vous savez qu'il s'agit d'une plateforme d'orchestration de conteneurs Open Source conçue pour exécuter des applications et des services distribués à grande échelle. En revanche, vous ne connaissez peut-être pas tous ses composants et ne savez pas comment ils interagissent entre eux.

Nous allons d'abord passer rapidement en revue les principes de conception derrière Kubernetes, puis découvrir comment les différents composants de Kubernetes fonctionnent ensemble.

\section{Principes de conception de Kubernetes}
Un cluster Kubernetes doit être :

\begin{itemize}
    \item Sécurisé : il doit suivre les bonnes pratiques les plus récentes en matière de sécurité.
    \item Facile à utiliser : quelques commandes simples doivent suffire à son fonctionnement.
    \item Adaptable : il ne doit pas favoriser un fournisseur et doit fournir un fichier de configuration permettant de le personnaliser.
\end{itemize}

\section{Quels sont les composants d'un cluster Kubernetes ?}

Le terme « cluster » désigne un déploiement fonctionnel de Kubernetes. Un cluster Kubernetes comprend deux principaux composants : le plan de contrôle et les machines de calcul ou nœuds. Chaque nœud est son propre environnement Linux®. Il peut s'agir d'une machine physique ou virtuelle. Chaque nœud exécute des pods, constitués de conteneurs.

Le schéma suivant représente les liens entre les différents composants d'un cluster Kubernetes :

\begin{figure}[h]
    \includegraphics[width=\textwidth]{kubernetes_diagram.png}
    \caption{Cluster Kubernetes}
\end{figure}

\section{Que se passe-t-il dans le plan de contrôle Kubernetes ?}

\subsection{Plan de contrôle}

Commençons par le centre névralgique de notre cluster Kubernetes : le plan de contrôle. Il contient les composants Kubernetes qui contrôlent le cluster, ainsi que des données sur l'état et la configuration du cluster. Ces principaux composants de Kubernetes s'assurent que suffisamment de conteneurs peuvent fonctionner avec les ressources nécessaires.

Le plan de contrôle est en contact permanent avec vos machines de calcul. Vous avez configuré votre cluster pour qu'il fonctionne d'une certaine manière. Le plan de contrôle s'assure que votre configuration est respectée.

\subsection{kube-apiserver}

Vous avez besoin d'interagir avec votre cluster Kubernetes ? C'est à cela que sert l'API. L'API de Kubernetes est la partie frontale du plan de contrôle. Elle prend en charge les demandes internes et externes. Le serveur d'API détermine si une demande est valide ou non et la traite, le cas échéant. Vous pouvez accéder à l'API avec des appels REST, à l'aide de l'interface en ligne de commande kubectl ou d'autres outils en ligne de commande tels que kubeadm.

\subsection{kube-scheduler}

Votre cluster est-il en bonne santé ? Est-il possible d'intégrer de nouveaux conteneurs si besoin ? Ce sont les questions auxquelles doit répondre le planificateur Kubernetes.

En plus de l'intégrité du cluster, le planificateur doit prendre en compte les besoins en ressources (par exemple, processeur ou mémoire) d'un pod. Il planifie ensuite l'attribution du pod au nœud de calcul adéquat.

\subsection{kube-controller-manager}

Les contrôleurs assurent l'exécution du cluster, tandis que le gestionnaire de contrôleur Kubernetes regroupe plusieurs fonctions de contrôleur. Un contrôleur se réfère au planificateur pour s'assurer qu'un nombre suffisant de pods est exécuté. Si un pod est défaillant, un autre contrôleur le remarque et réagit. Un contrôleur connecte les services aux pods afin que les demandes soient acheminées jusqu'aux points de terminaison appropriés. D'autres contrôleurs permettent de créer des comptes et des jetons d'accès aux API.

\subsection{etcd}

« etcd » est une base de données clé-valeur qui comprend les données de configuration et les informations sur l'état du cluster. Distribuée et résistante aux pannes, la base de données etcd constitue la référence unique concernant votre cluster.

\section{Que se passe-t-il au sein d'un nœud Kubernetes ?}

\subsection{Nœuds}

Un cluster Kubernetes requiert au moins un nœud de calcul, mais en général il en contient un grand nombre. Les pods sont planifiés et orchestrés pour être exécutés sur des nœuds. Vous avez besoin de faire évoluer la capacité de votre cluster ? Ajoutez des nœuds.

\subsection{Pods}

Le pod est l'unité la plus petite et la plus simple dans le modèle d'objets de Kubernetes. Il représente une instance unique d'une application. Chaque pod est constitué d'un conteneur ou d'une série de conteneurs étroitement couplés, ainsi que des options permettant de contrôler l'exécution de ces conteneurs. Il est possible de connecter les pods à un système de stockage persistant afin d'exécuter des applications avec état.

\subsection{Moteur d'exécution de conteneurs}

Chaque nœud de calcul dispose d'un moteur qui permet d'exécuter les conteneurs. Docker en est un exemple, mais Kubernetes prend en charge d'autres environnements conformes aux normes OCI (Open Container Initiative), tels que rkt et CRI-O.

\subsection{kubelet}

Chaque nœud de calcul contient un kubelet, une petite application qui communique avec le plan de contrôle. Le kubelet s'assure que les conteneurs sont exécutés dans un pod. Lorsque le plan de contrôle envoie une requête vers un nœud, le kubelet exécute l'action.

\subsection{kube-proxy}

Chaque nœud de calcul contient également un proxy réseau appelé « kube-proxy » qui facilite la mise en œuvre des services de mise en réseau de Kubernetes. Le composant kube-proxy gère les communications réseau dans et en dehors du cluster. Il utilise la couche de filtrage de paquets du système d'exploitation si elle est disponible, sinon il transmet le trafic lui-même.

\section{Quels sont les autres composants essentiels d'un cluster Kubernetes ?}

\subsection{Stockage persistant}

En plus des conteneurs qui exécutent une application, Kubernetes peut gérer les données d'application liées à un cluster. Un utilisateur peut demander des ressources de stockage, sans nécessairement connaître les détails de l'infrastructure de stockage sous-jacente. Les volumes persistants sont spécifiques à un cluster et non à un pod. Ils peuvent donc avoir une durée de vie supérieure à celle d'un pod.

\subsection{Registre de conteneurs}

Le registre de conteneurs stocke les images de conteneurs sur lesquelles repose Kubernetes. Il peut s'agir d'un registre tiers ou d'un registre que vous avez configuré vous-même.

\subsection{Infrastructure sous-jacente}

Vous pouvez choisir l'environnement d'exécution de Kubernetes : serveurs nus, machines virtuelles ou clouds publics, privés et hybrides. Kubernetes peut fonctionner sur de nombreux types d'infrastructures. C'est un avantage non négligeable.
